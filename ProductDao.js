function listProduct() {
    var arrayProduct = [];
    for (let index = 0; index < 5; index++) {
        let product = new Product(index, 'Nam' + index, 1, new Date(2021, 8, 20), 10, true);
        arrayProduct.push(product);
    }
    for (let index = 5; index < 10; index++) {
        let date = new Date();
        let product = new Product(index, 'Nga' + index, 2, new Date(2021, 1, 21), 10, false);
        arrayProduct.push(product);
    }
    return arrayProduct;
}

// filter name product's by id using ES6
function filterProductByIdES6(listProduct, idProduct) {
    return listProduct.filter(i => i.id==idProduct);
}

// filter name product's by id using For Loops Normal
function filterProductByIdFor(listProduct, idProduct) {
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].id == idProduct) {
            return listProduct[i];
        }
    }
}

// filter products has quanlity greater than 0 and not delete using ES6
function filterProductByQuanlityES6(listProduct) {
    return listProduct.filter(i => i.quality > 0 && i.isDelete==false);
}


// filter products has quanlity greater than 0 and not delete using For
function filterProductByQuanlityFor(listProduct) {
    var arrayProduct = [];
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].quality > 0 && listProduct[i].isDelete == false) {
            arrayProduct.push(listProduct[i]);
        }
    }
    return arrayProduct;
}

// filter products has Sale Date greater than Date Now using ES6
function filterProductBySaleDateES6(listProduct) {
    return listProduct.filter(i => i.saleDate > Date.now());
}

// filter products has Sale Date greater than Date Now using For
function filterProductBySaleDateFor(listProduct) {
    var arrayProduct = [];
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].saleDate > Date.now()) {
            arrayProduct.push(listProduct[i]);
        }
    }
    return arrayProduct;
}

//calculated total quantity product's using reduce
function totalProductReduce(listProduct) {
    const result = listProduct.reduce((accumulator, currentValue) => {
        if (currentValue.isDelete == true) {
            currentValue.quality = 0;
        }
        return accumulator + currentValue.quality;
    }, 0)
    return result;
}

//calculated total quantity product's using for
function totalProductFor(listProduct) {
    var result = 0;
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index].isDelete == false) {
            result += listProduct[index].quality;
        }
    }
    return result;
}


//check category is exist using ES6
function isHaveProductInCategoryES6(listProduct, categoryId) {

    if(listProduct.filter(i => i.categoryId== categoryId).length>0)
    return true;
}


//check category is exist using For
function isHaveProductInCategoryFor(listProduct, categoryId) {
    for (let index = 0; index < listProduct.length; index++) {
        if (listProduct[index].categoryId == categoryId) {
            return true;
        }
    }
}

// filter products has quanlity greater than 0 and Sale Date greater than Date Now using ES6
function filterProductBySaleDateProES6(listProduct) {
    return listProduct.filter(i => i.saleDate > Date.now() && i.quality > 0).map(i => { return [ i.id.toString() , i.name]});
}

// filter products has quanlity greater than 0 and  Sale Date greater than Date Now using For
function filterProductBySaleDateProFor(listProduct) {
    var arrayProduct = [];
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].saleDate > Date.now() && listProduct[i].quality > 0) {
            arrayProduct.push([listProduct[i].id.toString(), listProduct[i].name]);
        }
    }
    return arrayProduct;
}